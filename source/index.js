import React from 'react'
import { render } from 'react-dom'
import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import App from 'components/App'
import localStorageMiddleware, { getStoredState } from 'middleware/localStorage'
import 'style/redux-demo.less'

function counterReducer(state = 0, action) {
    switch (action.type) {
    case 'INCREMENT': return state + 1
    case 'DECREMENT': return state - 1
    case 'INCREMENT_IF_ODD': return (state % 2 === 1) ? state + 1 : state
    case 'SET_COUNTER': return action.value
    default: return state
    }
}

const rootReducer = combineReducers({
    counter: counterReducer
})

const finalCreateStore = compose(
    applyMiddleware(localStorageMiddleware, thunk),
    window.devToolsExtension()
)(createStore)
const initialState = getStoredState()
console.log(initialState)
const store = finalCreateStore(rootReducer, initialState)

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('redux-demo-application')
)
