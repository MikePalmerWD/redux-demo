import React from 'react'
import { connect } from 'react-redux'
import Button from 'react-bootstrap/lib/Button'
import { increment, decrement, incrementIfOdd, incrementAsync, save, reset } from '../ActionCreators'

class Controls extends React.Component {
    handleIncrement = () => {
        this.props.increment()
    }

    handleDecrement = () => {
        this.props.decrement()
    }

    handleIncrementIfOdd = () => {
        this.props.incrementIfOdd()
    }

    handleIncrementAsync = () => {
        this.props.incrementAsync()
    }

    handleSave = () => {
        this.props.save()
    }

    handleReset = () => {
        this.props.reset()
    }

    render() {
        return (
            <div className="fill-vertical"
                 style={{ padding: 30 }}>
                <div className="fill-vertical flex-container center-vertical center-horizontal align-vertical"
                     style={{ border: '1px solid #ccc' }}>
                    <Button style={{ margin: 5 }}
                            onClick={this.handleIncrement}>
                        {'Increment'}
                    </Button>
                    <Button style={{ margin: 5 }}
                            onClick={this.handleDecrement}>
                        {'Decrement'}
                    </Button>
                    <Button style={{ margin: 5 }}
                            onClick={this.handleIncrementIfOdd}>
                        {'Increment if odd'}
                    </Button>
                    <Button style={{ margin: 5 }}
                            onClick={this.handleIncrementAsync}>
                        {'Increment async'}
                    </Button>
                    <Button style={{ margin: 5 }}
                            onClick={this.handleSave}>
                        {'Save'}
                    </Button>
                    <Button style={{ margin: 5 }}
                            onClick={this.handleReset}>
                        {'Reset'}
                    </Button>
                </div>
            </div>
        )
    }
}

export default connect(null, { increment, decrement, incrementIfOdd, incrementAsync, save, reset })(Controls)
