import React from 'react'
import Col from 'react-bootstrap/lib/Col'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Controls from './Controls'
import Display from './Display'

export default class App extends React.Component {
    render() {
        return (
            <Grid className="fill-vertical">
                <Row className="fill-vertical">
                    <Col xs={4}
                         xsOffset={2}
                         className="fill-vertical">
                        <Controls/>
                    </Col>
                    <Col xs={4}
                         className="fill-vertical">
                        <Display/>
                    </Col>
                </Row>
            </Grid>
        )
    }
}
