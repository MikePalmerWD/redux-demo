import React from 'react'
import { connect } from 'react-redux'

const Display = ({ counter }) => (
    <div className="fill-vertical"
         style={{ padding: 30 }}>
        <div className="fill-vertical flex-container center-vertical center-horizontal align-vertical"
             style={{ border: '1px solid #ccc' }}>
            <span style={{ fontSize: 16 }}>{'Counter:'}</span>
            <span style={{ fontSize: 30 }}>{counter}</span>
        </div>
    </div>
)

function mapStateToProps(state) {
    return {
        counter: state.counter
    }
}

export default connect(mapStateToProps)(Display)
