import { WRITE_LOCAL_STORAGE } from 'middleware/localStorage'

export function increment() {
    return {
        type: 'INCREMENT'
    }
}

export function decrement() {
    return {
        type: 'DECREMENT'
    }
}

export function incrementIfOdd() {
    return {
        type: 'INCREMENT_IF_ODD'
    }
}

export function incrementAsync() {
    return dispatch => {
        setTimeout(() => dispatch(increment()), 2000)
    }
}

export function save() {
    return {
        [WRITE_LOCAL_STORAGE]: {
            types: ['SAVE_REQUEST', 'SAVE_SUCCESS', 'SAVE_FAILURE'],
            key: 'counter'
        }
    }
}

export function reset() {
    return {
        type: 'SET_COUNTER',
        value: 0
    }
}
