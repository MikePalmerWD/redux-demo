export const WRITE_LOCAL_STORAGE = 'WRITE_LOCAL_STORAGE'

export function getStoredState() {
    return JSON.parse(localStorage.getItem('redux-demo')) || {}
}

export default store => next => action => {
    const writeAction = action[WRITE_LOCAL_STORAGE]
    if (!writeAction) {
        return next(action)
    }

    const { key, types } = writeAction
    const [requestType, successType, failureType] = types

    next({ type: requestType, key })

    try {
        const storedState = getStoredState()
        const value = store.getState()[key]
        storedState[key] = value
        localStorage.setItem('redux-demo', JSON.stringify(storedState))
        return next({ type: successType, key, value })
    } catch (error) {
        return next({ type: failureType, error: error.message })
    }
}
