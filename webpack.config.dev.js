var path = require('path')
var autoprefixer = require('autoprefixer')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var webpack = require('webpack')

module.exports = {
    devtool: 'inline-source-map',

    entry: './source/index.js',

    output: {
        path: __dirname + '/build/src/',
        filename: 'main.js',
        publicPath: 'src/'
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['react-hot', 'babel']
            },
            {
                test: /\.(?:le|c)ss$/,
                loader: 'style!css!postcss!less'
            }
        ]
    },

    resolve: {
        root: path.resolve(__dirname, 'source')
    },

    postcss: [autoprefixer({ browsers: ['last 5 versions'] })],

    plugins: [
        new webpack.DefinePlugin({
            'process.env': { NODE_ENV: JSON.stringify('development') }
        }),
        new HtmlWebpackPlugin({
            inject: 'body',
            template: './source/index.html'
        })
    ]
}
