var webpack = require('webpack')
var WebpackDevServer = require('webpack-dev-server')

const Server = {
    host: 'localhost',
    port: 8080,
    address: function () {
        return `http://${this.host}:${this.port}`
    }
}

var devServerConfig = Object.create(require('./webpack.config.dev'))
devServerConfig.entry = [
    `webpack-dev-server/client?${Server.address()}`,
    'webpack/hot/only-dev-server',
    './source/index.js'
]
devServerConfig.output.publicPath = Server.address() + '/'
devServerConfig.plugins.unshift(new webpack.HotModuleReplacementPlugin())

var compiler = webpack(devServerConfig)
new WebpackDevServer(compiler, {
    hot: true,
    historyApiFallback: true,
    publicPath: devServerConfig.output.publicPath,
    stats: {
        colors: true
    }
}).listen(Server.port, Server.host, function (err) {
    if (err) {
        console.error(err)
    }

    console.log(`[webpack-dev-server] Listening at ${Server.address()}`)
})
